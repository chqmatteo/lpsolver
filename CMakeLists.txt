include(MCLabLibrary)

mclab_library(lpsolver 3.2.1)

mclab_find_and_link_library(${PROJECT_NAME} GLPK)
if(GLPK_FOUND)
  mclab_library_feature(GLPK)
endif()
mclab_find_and_link_library(${PROJECT_NAME} CPLEX)
if(CPLEX_FOUND)
  mclab_library_feature(CPLEX)
  mclab_pthreads(${PROJECT_NAME} REQUIRED)
endif()
mclab_find_and_link_library(${PROJECT_NAME} LPSOLVE)
if(LPSOLVE_FOUND)
  mclab_library_feature(LPSOLVE)
endif()

if(NOT GLPK_FOUND AND NOT CPLEX_FOUND AND NOT LPSOLVE_FOUND)
  message(FATAL_ERROR "There are no backends available!")
endif(NOT GLPK_FOUND AND NOT CPLEX_FOUND AND NOT LPSOLVE_FOUND)

mclab_generate_config_header()
