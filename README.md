# LPSOLVER

C library that provides an abstraction layer between the actual Linear Programming solver used and the user.

**It doesn't implement any LP solution algorithm itself**, instead it simply wraps already existing
solvers.

Currently supported backends:

- [GNU Linear Programming Kit](http://gnu.org/software/glpk/)
- [IBM ILOG CPLEX Optimizer](http://www.ibm.com/software/integration/optimization/cplex-optimizer/)
- [LP_SOLVE](http://lpsolve.sourceforge.net/)

The package is versioned using the [semantic versioning policy](http://semver.org).

## Building

This library building system is using CMake.

It relies on [additional find modules](https://bitbucket.org/mclab/cmake) for detecting backends.

```
mkdir build
cd build
cmake ..
make
```

You can also generate documentation via [doxygen](http://doxygen.org).

```
make doc
```
