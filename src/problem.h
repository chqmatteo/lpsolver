/*
 * Copyright (C) 2016 Vadim Alimguzhin
 *
 * This file is part of LPSOLVER open source library.
 *
 * LPSOLVER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LPSOLVER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROBLEM_H_
#define PROBLEM_H_

typedef struct LPSolverProblem LPSolverProblem;

enum LPSolverColType {
  LPSOLVER_COL_TYPE_C,
  LPSOLVER_COL_TYPE_I,
  LPSOLVER_COL_TYPE_B
};

typedef enum LPSolverColType LPSolverColType;

enum LPSolverColBoundsType {
  LPSOLVER_COL_BOUNDS_TYPE_FR,
  LPSOLVER_COL_BOUNDS_TYPE_FX,
  LPSOLVER_COL_BOUNDS_TYPE_LB,
  LPSOLVER_COL_BOUNDS_TYPE_UB,
  LPSOLVER_COL_BOUNDS_TYPE_DB
};

typedef enum LPSolverColBoundsType LPSolverColBoundsType;

enum LPSolverRowBoundsType {
  LPSOLVER_ROW_BOUNDS_TYPE_FX,
  LPSOLVER_ROW_BOUNDS_TYPE_LB,
  LPSOLVER_ROW_BOUNDS_TYPE_UB,
  LPSOLVER_ROW_BOUNDS_TYPE_DB
};

typedef enum LPSolverRowBoundsType LPSolverRowBoundsType;

enum LPSolverObjDir {
  LPSOLVER_OBJ_DIR_MIN,
  LPSOLVER_OBJ_DIR_MAX
};

typedef enum LPSolverObjDir LPSolverObjDir;

extern LPSolverProblem *
lpsolver_problem_new(LPSolver * /*solver*/, char const * /*name*/, int /*num_rows*/, int /*num_cols*/);

extern LPSolverProblem *
lpsolver_problem_new_from_file(LPSolver * /*solver*/, char const * /*filename*/);

extern void
lpsolver_problem_destroy(LPSolverProblem ** /*problem*/);

extern void
lpsolver_problem_and_solver_destroy(LPSolverProblem ** /*problem*/);

extern void
lpsolver_problem_write(LPSolverProblem const * /*problem*/, char const * /*filename*/);

extern int
lpsolver_problem_get_num_rows(LPSolverProblem const * /*problem*/);

extern int
lpsolver_problem_get_num_cols(LPSolverProblem const * /*problem*/);

extern LPSolverColType
lpsolver_problem_get_col_type(LPSolverProblem const * /*problem*/, int /*j*/);

extern void
lpsolver_problem_set_col_type(LPSolverProblem * /*problem*/, int /*j*/, LPSolverColType /*col_type*/);

extern double
lpsolver_problem_get_col_lb(LPSolverProblem const * /*problem*/, int /*j*/);

extern void
lpsolver_problem_set_col_lb(LPSolverProblem * /*problem*/, int /*j*/, double /*lb*/);

extern void
lpsolver_problem_set_col_bounds(LPSolverProblem * /*problem*/, int /*j*/, LPSolverColBoundsType /*bounds_type*/, double /*lb*/, double /*ub*/);

extern char *
lpsolver_problem_get_col_name(LPSolverProblem * /*problem*/, int /*j*/);

extern void
lpsolver_problem_set_col_name(LPSolverProblem * /*problem*/, int /*j*/, char const * /*name_fmt*/, ...);

extern int
lpsolver_problem_get_col_idx(LPSolverProblem const * /*problem*/, char const * /*name_fmt*/, ...);

extern int
lpsolver_problem_add_col(LPSolverProblem * /*problem*/, LPSolverColType /*col_type*/, LPSolverColBoundsType /*bounds_type*/, double /*lb*/, double /*ub*/, char const * /*name_fmt*/, ...);

extern double
lpsolver_problem_get_row_coeff(LPSolverProblem const * /*problem*/, int /*i*/, int /*j*/);

extern void
lpsolver_problem_set_row_coeff(LPSolverProblem * /*problem*/, int /*i*/, int /*j*/, double /*coeff*/);

extern LPSolverRowBoundsType
lpsolver_problem_get_row_bounds_type(LPSolverProblem const * /*problem*/, int /*i*/);

extern double
lpsolver_problem_get_row_lb(LPSolverProblem const * /*problem*/, int /*i*/);

extern double
lpsolver_problem_get_row_ub(LPSolverProblem const * /*problem*/, int /*i*/);

extern void
lpsolver_problem_set_row_bounds(LPSolverProblem * /*problem*/, int /*i*/, LPSolverRowBoundsType /*bounds_type*/, double /*lb*/, double /*ub*/);

extern void
lpsolver_problem_set_row_name(LPSolverProblem * /*problem*/, int /*i*/, char const * /*name_fmt*/, ...);

extern int
lpsolver_problem_get_row_idx(LPSolverProblem const * /*problem*/, char const * /*name_fmt*/, ...);

extern int
lpsolver_problem_add_row(LPSolverProblem * /*problem*/, LPSolverRowBoundsType /*bounds_type*/, double /*lb*/, double /*ub*/,  char const * /*name_fmt*/, ...);

extern LPSolverObjDir
lpsolver_problem_get_obj_dir(LPSolverProblem const * /*problem*/);

extern void
lpsolver_problem_set_obj_dir(LPSolverProblem * /*problem*/, LPSolverObjDir /*dir*/);

extern double
lpsolver_problem_get_obj_coeff(LPSolverProblem const * /*problem*/, int /*j*/);

extern void
lpsolver_problem_set_obj_coeff(LPSolverProblem * /*problem*/, int /*j*/, double /*coeff*/);

extern double
lpsolver_problem_get_obj_offset(LPSolverProblem const * /*problem*/);

extern void
lpsolver_problem_set_obj_offset(LPSolverProblem * /*problem*/, double /*offset*/);

extern double
lpsolver_problem_get_obj_coeff_quad(LPSolverProblem const * /*problem*/, int /*j1*/, int /*j2*/);

extern void
lpsolver_problem_set_obj_coeff_quad(LPSolverProblem * /*problem*/, int /*j1*/, int /*j2*/, double /*coeff*/);

extern bool
lpsolver_problem_optimize(LPSolverProblem * /*problem*/);

extern double
lpsolver_problem_get_obj_value(LPSolverProblem const * /*problem*/);

extern double
lpsolver_problem_get_col_value(LPSolverProblem const * /*problem*/, int /*j*/);

#endif /* #ifndef PROBLEM_H_ */
