/*
 * Copyright (C) 2016 Vadim Alimguzhin
 *
 * This file is part of LPSOLVER open source library.
 *
 * LPSOLVER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LPSOLVER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LPSOLVER_H_
#define LPSOLVER_H_

enum LPSolverBackendType {
  LPSOLVER_BACKEND_TYPE_GLPK,
  LPSOLVER_BACKEND_TYPE_LP_SOLVE,
  LPSOLVER_BACKEND_TYPE_CPLEX
};

typedef enum LPSolverBackendType LPSolverBackendType;

typedef struct LPSolver LPSolver;

struct LPSolver {
  LPSolverBackendType backend_type;
  union {
    GLPK_SOLVER_DEF
    LP_SOLVE_SOLVER_DEF
    CPLEX_SOLVER_DEF
  } backend;
};

extern bool
lpsolver_has_backend(LPSolverBackendType /*backend_type*/);

extern LPSolver *
lpsolver_new(LPSolverBackendType /*backend_type*/);

extern void
lpsolver_destroy(LPSolver ** /*solver*/);

extern void
lpsolver_set_num_threads(LPSolver * /*solver*/, int /*num_threads*/);

#endif /* #ifndef LPSOLVER_H_ */
