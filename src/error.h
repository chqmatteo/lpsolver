/*
 * Copyright (C) 2016 Vadim Alimguzhin
 *
 * This file is part of LPSOLVER open source library.
 *
 * LPSOLVER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LPSOLVER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ERROR_H_
#define ERROR_H_

#define lpsolver_fatalerror(condition, ...) { \
  if (condition) { \
    fprintf(stderr, "Fatal error (" PACKAGE "): "); \
    fprintf(stderr, __VA_ARGS__); \
    fprintf(stderr, "\n"); \
    exit(EXIT_FAILURE); \
  } \
}

#endif /* #ifndef PROBLEM_H_ */
