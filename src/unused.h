#ifndef UNUSED_H_
#define UNUSED_H_

#ifdef __GNUC__
#define UNUSED(x) __attribute__((unused)) x
#else /* #ifdef __GNUC__ */
#define UNUSED(x) x
#endif /* #ifdef __GNUC__ */

#endif /* #ifndef UNUSED_H_ */
